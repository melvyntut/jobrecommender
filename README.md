# Job Recommender System

These files contain an algorithm written in Python that uses .json files for data.

## Installation

Before installing, make sure you have the latest version of Python installed on your system. It can be downloaded from https://www.python.org/downloads/

## Usage

To run the program, open Command Prompt (or your operating system's equivalent) and navigate to the directory of the folder where you installed the files. Then type in "python algorithm.py" and the program should run

