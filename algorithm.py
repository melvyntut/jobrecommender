import json

#import the json data
with open('jobs_subset.json', 'r') as infile:
    jobs_array = json.load(infile)
    
with open('users.json', 'r') as infile:
    users_array = json.load(infile)


for user in users_array:

    #creating arrays for soft and hard skills   
    softSkills = []
    hardSkills = []
    
    #prints the user's username
    print("Username: " + user["username"])
    
    #adding soft skills to softSkills array
    for skill in user["soft_skills"]:
        softSkills.append(skill)
    
    #prints the amount of soft skills, and a list of the soft skills (FOR TESTING PURPOSES)
    print("---------------------------------------")
    print("Number of soft skills: " + str(len(user["soft_skills"])))
    print(softSkills)
    
    #adding hard skills to the hardSkills array
    for skill in user["hard_skills"]:
          hardSkills.append(skill)
    
    #prints the user's amount of hard skills and a list of the hard skills (FOR TESTING PURPOSES)
    print("Number of hard skills: " + str(len(user["hard_skills"])))
    print(hardSkills)
    
    print("")
    
    #going through each job
    for job in jobs_array:
    
        print(job["title"])
        
        print("Soft skills:")
        softMatches = 0
        
        #for each skill in the job's soft skills
        for skill in job["soft_skills"]:
        
            i = 0
            
            """
            This while loop goes through each skill in the softSkills array. For testing purposes, it prints out
            the user's skill and the skill in the array that it's being compared to. If they match, "MATCH" is
            printed to the console (for testing) and 1 is added to the amount of soft matches. If don't match,
            "NO MATCH" is printed to the console. The loop uses the variabe i that increments in each iteration.
            """
            while i < len(softSkills):
                if skill == softSkills[i]:
                    print(skill + " + " + softSkills[i] + " --> MATCH")
                    i = i + 1
                    softMatches = softMatches + 1
                else:
                    print(skill + " + " + softSkills[i] + " --> no match")
                    i = i + 1
        
        #prints out the amount of soft skill matches (for testing purposes)
        print("Soft matches: " + str(softMatches))
        
        print("")
            
        print("Hard skills:")
        hardMatches = 0
        
        #for each skill in the job's hard skills
        for skill in job["hard_skills"]:
        
            i = 0
            
            """
            This while loop does the same as the soft skills one, but it does it for hard skills.
            """
            while i < len(hardSkills):
                #print(skill + " " + hardSkills[i])
                if skill == hardSkills[i]:
                    print(skill + " + " + hardSkills[i] + " --> MATCH")
                    i = i + 1
                    hardMatches = hardMatches + 1
                else:
                    print(skill + " + " + hardSkills[i] + " --> no match")
                    i = i + 1
        
        #prints out the amount of hard skill matches (for testing purposes)
        print("Hard matches: " + str(hardMatches))
        
        """
        Hard skills are worth two points, soft skills are worth one. The calculation below takes the amount of hard skills
        and multiplies it by two, then adds that result to the amount of soft matches to calculate the total score.
        """
        job["score"] = (hardMatches * 2) + softMatches
        print("Job score: " + str(job["score"]))
        
        print("")

    
    """
    The for loop works out the largest score of all the scores collected
    and assigns that value to the biggestScore variable. It then prints
    out each job for each user based on its job score in descending order
    (excluding the jobs that have a score of 0).
    """
    
    biggestScore = 1

    for job in jobs_array:
        if job["score"] > 0:
            if job["score"] > biggestScore:
                biggestScore = job["score"]

    print("---------------------------------------------")
    print("RESULTS FOR: " + user["username"])
    print("")

    while biggestScore > 0:
        for job in jobs_array:
            if job["score"] > 0:
                if job["score"] == biggestScore:
                    print(job["title"])
                    print(job["text"])
                    #prints the score for that job (testing purposes)
                    print("Score for job: " + str(job["score"]))
                    print("")
                    
        biggestScore = biggestScore - 1




