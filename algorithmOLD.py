import json

#import the json data
with open('jobs_subset.json', 'r') as infile:
    jobs_array = json.load(infile)
    
with open('users.json', 'r') as infile:
    users_array = json.load(infile)
    
#ask for up to ten skills
userSkills = []
i = 0
numberOfSkillsAdded = 0;
print("You can enter up to ten skills")
while i < 10:
    userInput = input("Enter a skill: ")
    if userInput == "":
        i = i + 10
    else:
        userSkills.append(userInput)
        i = i + 1
        numberOfSkillsAdded = numberOfSkillsAdded + 1;
        
#print out the skills added
print("You entered:")
for skill in userSkills:
    print(skill)
    
print("")
    
#search through, return jobs that match
#if job score 
for job in jobs_array:
    print("Job: " + job["title"])
    print("Amount of hard skills in this job: " + str(len(job["hard_skills"])))
    for skill in job["hard_skills"]:
        i = 0
        while i < numberOfSkillsAdded:
            if skill == userSkills[i]:
                job["score"] = job["score"] + 2
                print("MATCH")
                """print(job["title"])
                print(job["text"])"""
                i = i + 1
            else:
                print("No match for skill " + str(i))
                i = i + 1
                
    print("Amount of soft skills in this job: " + str(len(job["soft_skills"])))
    for skill in job["soft_skills"]:
        i = 0
        while i < numberOfSkillsAdded:
            if skill == userSkills[i]:
                job["score"] = job["score"] + 1
                print("MATCH")
                """print(job["title"])
                print(job["text"])"""
                i = i + 1
            else:
                print("No match for skill " + str(i))
                i = i + 1
    
    print("JOB SCORE: ")
    print(job["score"])
                
    print("")
    
biggestScore = 1

for job in jobs_array:
    if job["score"] > 0:
        if job["score"] > biggestScore:
            biggestScore = job["score"]

print("The biggest score is:")          
print(biggestScore)
print("RESULTS")
print("")

#print out the jobs in descending order based on job score
biggestScore = 0
while biggestScore > 0:
    for job in jobs_array:
        if job["score"] > 0:
            if job["score"] == biggestScore:
                print(job["title"])
                print(job["text"])
                print(job["score"])
                print("")
            else:
                print("Nope")
                print(biggestScore)
    biggestScore = biggestScore - 1
    
        
    
    
    
            
"""


"""